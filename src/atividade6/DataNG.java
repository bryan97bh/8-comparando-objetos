package atividade6;

public class DataNG implements IData {

	private int dias;

	public DataNG() {
		setAno((short) 1);
		setMes((byte) 1);
		setDia((byte) 1);
	}

	public DataNG(int dia, int mes, int ano) {
		this();
		setAno((short) ano);
		setMes((byte) mes);
		setDia((byte) dia);
	}

	@Override
	public boolean ehBissexto(short ano) {
		return (ano % 400 == 0) || ((ano % 4 == 0) && (ano % 100 != 0));
	}

	@Override
	public byte getUltimoDia(byte mes, short ano) {
		byte ud[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (mes == 2 && ehBissexto(ano)) {
			return 29;
		}

		return ud[mes];
	}

	@Override
	public byte getDia() {
		return (byte) converterDias()[0];
	}

	@Override
	public void setDia(byte dia) {
		int[] data = converterDias();
		byte ultimoDia = getUltimoDia((byte) data[1], (short) data[2]);

		if (dia >= 1 && dia <= ultimoDia) {
			this.dias = (this.dias - data[0]) + dia;
		}
	}

	@Override
	public byte getMes() {
		return (byte) converterDias()[1];
	}

	@Override
	public void setMes(byte mes) {
		int[] data = converterDias();
		if (mes >= 1 && mes <= 12) {
			this.dias = (this.dias - data[1] * 30) + mes * 30;
		}
	}

	@Override
	public short getAno() {
		return (short) converterDias()[2];
	}

	@Override
	public void setAno(short ano) {
		int[] data = converterDias();
		if (ano >= 1 && ano <= 9999) {
			this.dias = (this.dias - data[2] * 365) + ano * 365;
		}
	}

	@Override
	public void incrementaDia() {
		int[] data = converterDias();
		byte ultimoDia = getUltimoDia((byte) data[1], (short) data[2]);

		byte aux = (byte) (data[0] + 1);

		if (aux == (ultimoDia + 1)) {
			setDia((byte) 1);
			incrementaMes();
		} else {
			setDia(aux);
		}

	}

	@Override
	public void incrementaMes() {
		int[] data = converterDias();

		byte aux = (byte) (data[1] + 1);

		if (aux == 13) {
			setMes((byte) 1);
			incrementaAno();
		} else {
			setMes(aux);
		}

	}

	@Override
	public void incrementaAno() {
		int[] data = converterDias();

		short aux = (short) (data[2] + 1);

		if (aux == 10000) {
			setAno((short) 1);
		} else {
			setAno(aux);
		}

	}

	private int[] converterDias() {
		int ano = dias / 365;
		int mes = (dias - ano * 365) / 30;

		if (mes == 0) {
			ano -= 1;
			mes = 12;
		}

		int dia = (dias - ano * 365) - mes * 30;

		int[] data = { dia, mes, ano };
		return data;
	}

	@Override()
	public String toString() {
		return String.format("%02d/%02d/%02d", getDia(), getMes(), getAno());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataNG other = (DataNG) obj;	
		if(other.getAno() != other.getAno()) 
			return false;
		if(other.getMes() != other.getMes()) 
			return false;
		if(other.getDia() != other.getDia()) 
			return false;
		return true;
	}
}
