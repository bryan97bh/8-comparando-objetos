package atividade6;

public interface IData {
	boolean ehBissexto(short ano);
	byte getUltimoDia(byte mes, short ano);
	byte getDia();
	void setDia(byte dia);
	byte getMes();
	void setMes(byte mes);
	short getAno();
	void setAno(short ano);
	void incrementaDia();
	void incrementaMes();
	void incrementaAno();
	String toString();
}
